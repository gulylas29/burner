# Burner

[![pipeline status](https://gitlab.com/uraharashop/burner/badges/master/pipeline.svg)](https://gitlab.com/uraharashop/burner/commits/master)
[![coverage report](https://gitlab.com/uraharashop/burner/badges/master/coverage.svg)](https://gitlab.com/uraharashop/burner/commits/master)

## Flags

`-mode`

Mode of the encoding.

Possible values: `smp4`, `fmp4`, `mp4`, `transcode`

If no value is set then it has to be set in an input.

---

`-input`

Folder of the input files.

Default: `./in`

---

`-output`

Folder of the output files.

Default: `./out`

---

`-ffmpeg`

Path to FFmpeg executable.

---

`-v`

Make output verbose.

Default: `false`

---

`-w`

Wait for user input before close.

Default: `false`

---

`-v-height`

Target video height.

Default: `720`

---

`-v-bitrate`

Target video bitrate.

Default: `1371k`

---

`-v-upscaling`

Enable/disable upscaling.

Default: `false`
