package ffmpeg

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var skippingFontError = false

var (
	// arialFont matches a missing font command line from FFmpeg
	//
	// libass replaces a missing font with Arial when a default font is not specified
	// https://github.com/libass/libass/blob/0527f023f764976a6848b54af542c1141ae3f09e/libass/ass_fontselect.c#L714
	arialFont = regexp.MustCompile(`\[Parsed_subtitles_0 @ \w+] fontselect: \((.*?), \d+, \d+\) -> .*?, \d+, (?:ArialMT|Arial-BoldMT|Arial-ItalicMT)$`)
	// dejaVuSansFont matches a missing font command line from FFmpeg
	//
	// libass replaces a missing font with DejaVuSans on Linux when a default font is not specified
	dejaVuSansFont = regexp.MustCompile(`\[Parsed_subtitles_0 @ \w+] fontselect: \((.*?), \d+, \d+\) -> .*?, \d+, (?:DejaVuSans)$`)

	glyphError = regexp.MustCompile(`\[Parsed_subtitles_0 @ \w+] Glyph 0x(.*?) not found, selecting one more font for \((.*?), \d+, \d+\)`)

	hungarianSpecificChars = []rune{'ő', 'Ő', 'ű', 'Ű', 'ó', 'Ó', 'á', 'Á', 'é', 'É', 'ú', 'Ú', 'í', 'Í', 'Ü', 'ü', 'Ö', 'ö'}
)

func ValidateFont(cmd string) error {
	var match []string
	match = arialFont.FindStringSubmatch(cmd)
	if len(match) > 0 && !strings.HasPrefix(match[1], "Arial") {
		if !skippingFontError {
			return fmt.Errorf("missing font: %s", match[1])
		} else {
			fmt.Printf("Skipping fonterror\n")
			skippingFontError = false
		}
	}
	match = dejaVuSansFont.FindStringSubmatch(cmd)
	if len(match) > 0 && !strings.HasPrefix(match[1], "DejaVuSans") {
		if !skippingFontError {
			return fmt.Errorf("missing font: %s", match[1])
		} else {
			fmt.Printf("\nSkipping fonterror\n")
			skippingFontError = false
		}
	}
	match = glyphError.FindStringSubmatch(cmd)
	if len(match) > 1 {
		fmt.Printf("\nGlyph not found for: 0x%s ", match[1])
		char, err := strconv.ParseInt(match[1], 16, 64)
		if err != nil {
			fmt.Printf("\nCan't convert 0x%s to unicode character\n", match[1])
		} else {
			if err := checkGlyphFatalError(rune(char)); err != nil {
				return fmt.Errorf("Glyph error: %s in font '%s'\n", err, match[2])
			}
			fmt.Printf("Which is in unicode: %s", string(char))
		}
		skippingFontError = true
	}
	return nil
}

func checkGlyphFatalError(data rune) error {
	for _, i := range hungarianSpecificChars {
		if i == data {
			return fmt.Errorf("Hungarian char '%s' not found ", string(i))
		}
	}
	return nil
}
