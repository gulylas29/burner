package ffmpeg

import "testing"

func TestValidateFont(t *testing.T) {
	type args struct {
		cmd string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "font replaced with Arial",
			args:    args{cmd: "[Parsed_subtitles_0 @ anyhex] fontselect: (HZsH_Xirwena, 400, 0) -> ArialMT, 0, ArialMT"},
			wantErr: true,
		},
		{
			name:    "font replaced with DejaVuSans",
			args:    args{cmd: "[Parsed_subtitles_0 @ anyhex] fontselect: (HZsH_Xirwena, 400, 0) -> /font/path/DejaVuSans.ttf, 0, DejaVuSans"},
			wantErr: true,
		},
		{
			name:    "font is Arial",
			args:    args{cmd: "[Parsed_subtitles_0 @ anyhex] fontselect: (Arial, 400, 0) -> ArialMT, 0, ArialMT"},
			wantErr: false,
		},
		{
			name:    "font is DejaVuSans",
			args:    args{cmd: "[Parsed_subtitles_0 @ anyhex] fontselect: (DejaVuSans, 400, 0) -> /font/path/DejaVuSans.ttf, 0, DejaVuSans"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateFont(tt.args.cmd); (err != nil) != tt.wantErr {
				t.Errorf("ValidateFont() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
