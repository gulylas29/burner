package mode

import (
	"io"
	"strconv"
)

const (
	None Mode = iota - 1
	SampleMP4
	FragmentedMP4
	MP4
	Transcode
)

var (
	Modes = []Mode{
		FragmentedMP4,
		MP4,
		Transcode,
		SampleMP4,
	}

	labels = map[Mode]string{
		SampleMP4:     "Sample MP4 (mux)",
		FragmentedMP4: "Fragmented MP4 (HLS)",
		MP4:           "MP4 (mux)",
		Transcode:     "Transcode (softsub)",
	}

	flags = map[string]Mode{
		"smp4":      SampleMP4,
		"fmp4":      FragmentedMP4,
		"mp4":       MP4,
		"transcode": Transcode,
	}
)

type Mode int

// Label is a user friendly representation of m
func (m Mode) Label() string {
	return labels[m]
}

// FromString recognises a string representation of
// modes.
//
// If the string is unknown it returns None.
func FromString(s string) Mode {
	for k, m := range flags {
		if k == s {
			return m
		}
	}
	return None
}

// FromReader reads a rune from the given reader which
// value will be used to determine a Mode.
//
// If the rune is unknown it returns None.
func FromReader(reader io.RuneReader) Mode {
	r, _, _ := reader.ReadRune()
	i, err := strconv.Atoi(string(r))
	if err != nil {
		return None
	}
	for _, m := range Modes {
		if int(m) == i {
			return m
		}
	}
	return None
}
