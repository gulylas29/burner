package mode

import (
	"io"
	"strings"
	"testing"
)

func TestFromString(t *testing.T) {
	type args struct {
		m string
	}
	tests := []struct {
		name string
		args args
		want Mode
	}{
		{
			name: "invalid mode",
			args: args{m: ""},
			want: None,
		},
		{
			name: "existing mode",
			args: args{m: "smp4"},
			want: SampleMP4,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromString(tt.args.m); got != tt.want {
				t.Errorf("FromString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFromReader(t *testing.T) {
	type args struct {
		reader io.RuneReader
	}
	tests := []struct {
		name string
		args args
		want Mode
	}{
		{
			name: "empty string",
			args: args{reader: strings.NewReader("")},
			want: None,
		},
		{
			name: "invalid num",
			args: args{reader: strings.NewReader("a")},
			want: None,
		},
		{
			name: "invalid mode",
			args: args{reader: strings.NewReader("9")},
			want: None,
		},
		{
			name: "existing mode",
			args: args{reader: strings.NewReader("0")},
			want: SampleMP4,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromReader(tt.args.reader); got != tt.want {
				t.Errorf("FromReader() = %v, want %v", got, tt.want)
			}
		})
	}
}
