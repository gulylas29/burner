package main

import (
	"bufio"
	"bytes"
	"container/ring"
	"flag"
	"fmt"
	"gitlab.com/uraharashop/burner/ffmpeg"
	"gitlab.com/uraharashop/burner/filepathutil"
	"gitlab.com/uraharashop/burner/mode"
	"gitlab.com/uraharashop/burner/osutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var (
	version = "v0.0.0-SNAPSHOT"

	showVersion = flag.Bool("version", false, "show version information")

	// supportedInputExt filters the files from the input directory
	supportedInputExt = []string{".mkv", ".mp4", ".avs"}

	verboseOutput          = flag.Bool("v", false, "make output verbose")
	waitForUserBeforeClose = flag.Bool("w", false, "wait for user input before close")

	modeFlag = flag.String("mode", "", `mode of the encoding

Possible values:
 - smp4
   Stands for Sample MP4. Encodes a sample with the subtitle burned on the video. Creates hardsub.
 - fmp4
   Stands for Fragmented MP4. Encodes a fragmented video (HLS) with the subtitle burned on the video. Creates hardsub.
 - mp4
   Stands for MP4. Encodes a video with the subtitle burned on the video. Creates hardsub.
 - transcode
   Stands for Transcode. Encodes a video with the given options while keeping the original settings. Creates softsub.`)

	inputFolder  = flag.String("input", "./in", "folder of the input files")
	outputFolder = flag.String("output", "./out", "folder of the input files")
	ffmpegBinary = flag.String("ffmpeg", "", "path to FFmpeg executable")

	videoHeight    = flag.Int("v-height", 720, "target video height")
	videoBitrate   = flag.String("v-bitrate", "1371k", "target video bitrate")
	videoUpscaling = flag.Bool("v-upscaling", false, "enable/disable upscaling")
)

func main() {
	flag.Parse()

	fmt.Println("Burner", version)
	if *showVersion {
		return
	}

	reader := bufio.NewReader(os.Stdin)

	defer func() {
		if *waitForUserBeforeClose {
			fmt.Println("Press any key to exit")
			_, _, _ = reader.ReadRune()
		}
	}()

	selectedMode := mode.FromString(*modeFlag)
	for selectedMode == mode.None {
		fmt.Println("Select mode:")
		for _, m := range mode.Modes {
			fmt.Println(fmt.Sprintf("[%d] %s", m, m.Label()))
		}

		selectedMode = mode.FromReader(reader)
	}

	ffmpegExecuteable, err := osutil.LocateExecutable("ffmpeg", *ffmpegBinary)
	if err != nil {
		log.Print(err)
		return
	}
	if *verboseOutput {
		log.Print(ffmpegExecuteable)
	}

	absIn, err := filepath.Abs(*inputFolder)
	if err != nil {
		log.Print("Could not create absolute representation of input folder")
		return
	}
	absOut, err := filepath.Abs(*outputFolder)
	if err != nil {
		log.Print("Could not create absolute representation of output folder")
		return
	}

	files := filepathutil.ListFilesWithExt(absIn, supportedInputExt...)
	l := len(files)

	for i, file := range files {
		log.Printf("[%03d/%03d] %s", i+1, l, filepath.Base(file))

		// For YUV 4:2:0 chroma subsampled outputs width and height has to be divisible by 2
		f := ffmpeg.Filter{Subtitle: file, Width: -2, Height: *videoHeight, Upscaling: *videoUpscaling}

		var t *ffmpeg.Transcoder
		switch selectedMode {
		case mode.SampleMP4:
			t = ffmpeg.NewPresetTranscoderForSampleMp4(ffmpegExecuteable, file, absOut, *videoBitrate, f)
		case mode.FragmentedMP4:
			t = ffmpeg.NewPresetTranscoderForFragmentedMp4(ffmpegExecuteable, file, absOut, *videoBitrate, f)
		case mode.MP4:
			t = ffmpeg.NewPresetTranscoderForMp4(ffmpegExecuteable, file, absOut, *videoBitrate, f)
		case mode.Transcode:
			t = ffmpeg.NewPresetTranscoder(ffmpegExecuteable, file, absOut, *videoBitrate, f)
		default:
			continue
		}

		err := os.MkdirAll(t.OutDir(), 0755)
		if err != nil {
			log.Print(err)
			continue
		}

		if err := RunCommand(t.FirstPass(), *verboseOutput); err != nil {
			log.Print(err)
			continue
		}

		if err := RunCommand(t.SecondPass(), *verboseOutput); err != nil {
			log.Print(err)
			continue
		}

		// Remove FFmpeg logs
		_ = os.Remove(filepath.Join(t.OutDir(), "ffmpeg2pass-0.log"))
		_ = os.Remove(filepath.Join(t.OutDir(), "ffmpeg2pass-0.log.mbtree"))
	}
}

// RunCommand runs the given command while writing the output to console.
//
// The verbose argument makes the output more talkative.
func RunCommand(cmd *exec.Cmd, verbose bool) error {
	// For some reason FFmpeg writes to stderr
	e, err := cmd.StderrPipe()
	if err != nil {
		return err
	}
	if verbose {
		fmt.Println(cmd)
	}
	if err := cmd.Start(); err != nil {
		return err
	}

	scanner := bufio.NewScanner(e)
	scanner.Split(ScanLineAndCarriageReturn)
	prefix := "frame="
	needsNewLine := false
	defer func() {
		if needsNewLine {
			needsNewLine = false
			fmt.Println()
		}
	}()
	cbuf := ring.New(5)
	fmt.Println()
	for scanner.Scan() {
		m := scanner.Text()
		if strings.HasPrefix(m, prefix) {
			needsNewLine = true
			fmt.Print("\r" + m) // Override the previous line
			continue
		}
		if err := ffmpeg.ValidateFont(m); err != nil {
			_ = cmd.Process.Kill()
			return fmt.Errorf("burner: %w", err)
		} else if verbose {
			if needsNewLine {
				needsNewLine = false
				fmt.Println()
			}
			fmt.Println(m)
		} else if strings.HasSuffix(m, "Not overwriting - exiting") {
			i := strings.LastIndex(m, ".") + 1
			_ = cmd.Process.Kill()
			return fmt.Errorf("burner: %s", m[:i])
		} else {
			cbuf.Value = m
			cbuf = cbuf.Next()
		}
	}
	if needsNewLine {
		needsNewLine = false
		fmt.Println()
	}

	if err := cmd.Wait(); err != nil {
		cbuf.Do(func(i interface{}) {
			if i != nil {
				fmt.Println(fmt.Sprintf("%s", i))
			}
		})
		return err
	}
	return nil
}

// ScanLineAndCarriageReturn is a split function for a Scanner that returns
// each line of text, stripped of any trailing end-of-line marker or carriage
// return. The returned line may be empty. The end-of-line marker is one
// optional carriage return followed by one mandatory newline. In regular
// expression notation, it is `(\r|\r?\n)`. The last non-empty line of input
// will be returned even if it has no newline.
//
// Carriage return is used when a command overrides a single line of output.
func ScanLineAndCarriageReturn(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := bytes.IndexByte(data, '\n'); i >= 0 {
		// We have a full newline-terminated line.
		return i + 1, dropCR(data[0:i]), nil
	}
	// If we're at EOF, we have a final, non-terminated line. Return it.
	if atEOF {
		return len(data), dropCR(data), nil
	}
	if i := bytes.IndexByte(data, '\r'); i >= 0 {
		// We have a full newline-terminated line.
		return i + 1, data[0:i], nil
	}
	// Request more data.
	return 0, nil, nil
}

// dropCR drops a terminal \r from the data.
//
// Borrowed from bufio/scan
func dropCR(data []byte) []byte {
	if len(data) > 0 && data[len(data)-1] == '\r' {
		return data[0 : len(data)-1]
	}
	return data
}
