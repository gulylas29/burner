package osutil

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
)

// LocateExecutable returns an absolute path for the given command.
//
// If an alternative path is given it is prioritized over the default command.
func LocateExecutable(command, alternativePath string) (string, error) {
	pathOrExecutable, err := exec.LookPath(command)
	switch {
	case alternativePath != "":
		info, err := os.Stat(alternativePath)
		if os.IsNotExist(err) {
			return "", fmt.Errorf("given path for %s does not exists", command)
		}
		if info.IsDir() {
			alternativePath = filepath.Join(alternativePath, command)
		}
		return filepath.Abs(alternativePath)
	case err != nil:
		return "", fmt.Errorf("%s is not found", command)
	}
	abs, err := filepath.Abs(pathOrExecutable)
	if err != nil {
		return "", err
	}
	return abs, nil
}
